export interface Root {
  Medicos: Medicos[]
}

export interface Medicos {
  id: number
  shortName: string
  name: string
  image: string
  template: string
  created: string
  updated: string
  plan: string
  culture: string
  analytics: Analytics
  starred?: boolean
}

export interface Analytics {
  patient: Patient
  attendance: Attendance
}

export interface Patient {
  total: number
  actived: number
}

export interface Attendance {
  finished: number
  scheduled: number
}