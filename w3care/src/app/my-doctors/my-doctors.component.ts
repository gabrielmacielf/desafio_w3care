import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Medicos, Patient } from './medicos';
import { fromEvent } from 'rxjs';
import { SelectItem } from 'primeng/api';


@Component({
  selector: 'app-my-doctors',
  templateUrl: './my-doctors.component.html',
  styleUrls: ['./my-doctors.component.css']
})
export class MyDoctorsComponent implements OnInit {

  medicos: Medicos[];
  patient: Patient[];
  sortOptions: SelectItem[] = [];
  sortOrder: number = -1;
  sortField: string = "";
  sortKey: string = "";

  constructor(private service: AppService) {
    this.medicos = [];
    this.patient = [];
  }

  ngOnInit(): void {
    this.service.list().subscribe(data => this.medicos = data);

    this.sortOptions = [
      { label: 'Sort by name', value: 'name' },
      { label: 'Sort by created', value: 'created' }
    ];

  }

  onSortChange(field: string) {
    this.sortOrder = 0;
    this.sortField = '';
    setTimeout(() => {
      let value = field;
      if (value == 'starred') {
        this.sortOrder = -1;
      }
      else {
        this.sortOrder = 1;
      }
      this.sortField = value;
    }, 100);

  }
}
