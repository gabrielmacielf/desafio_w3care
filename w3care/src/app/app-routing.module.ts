import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyDoctorsComponent } from './my-doctors/my-doctors.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  {
    path: '',
    component: MyDoctorsComponent,
  },
  {
    path: 'profile/:id',
    component: ProfileComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule,]
})
export class AppRoutingModule { }
