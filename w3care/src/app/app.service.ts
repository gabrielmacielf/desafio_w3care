import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Medicos } from './my-doctors/medicos';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  private readonly API = 'http://localhost:3000/Medicos';
  //medicosURL='http://localhost:3000/Medicos'

  constructor(private http: HttpClient) { }
  list(){
    return this.http.get<Medicos[]>(this.API)
  }
}
