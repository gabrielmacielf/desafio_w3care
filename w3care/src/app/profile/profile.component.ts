import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';
import { Medicos } from '../my-doctors/medicos';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  id: number = 0;
  medico: Medicos | undefined;

  constructor(private route: ActivatedRoute, private service: AppService) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get("id"));
    this.service.list().subscribe(data => {
      this.medico = data.find(medico => { return medico.id == this.id })
    });
  }

}
